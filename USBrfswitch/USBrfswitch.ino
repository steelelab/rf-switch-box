//////////////////////////////////////////////////////////////////////////////////////////////////
// USB controler for Radial RFswitch
// By Erik van der Wiel
// TU-delft DEMO
// Date: 09-04-2021
// Date lastupdate: 22-04-2021
// Version: V1.1
//
// Description:
// Can ceive a command over serialport and execute this command to drive the latching coil of a Radial RF switch.
// Commands:
// AT
// CH1
// CH2
// CH3
// CH4
// CH5
// CH6
// CH_RESET
//
//
// A command needs to be preceded bij an AT command
// A Newline character denotes the end of a command string
//
// Example to switch on Channel 1 of the RFswitch:
// AT(newline)
// CH1(newline)
//
// Example to reset all switches:
// AT(newline)
// CH_RESET(newline)
//
// All command are case insensitve
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#define VERSION "V1.1"

// Define pinnumbers to RF channels and reset
#define CH1 2
#define CH2 3
#define CH3 4
#define CH4 5
#define CH5 6
#define CH_RESET 7
#define CH6 8

// Pulsetime in ms
#define PULSE 10

const byte numChars = 32;       // Max size of text command
char receivedChars[numChars];   // Databuffer array foor receiving cammand by serial port
char command[numChars];         // Buffer to store command
char lastcommand[numChars];     // kBuffer to hold previous command

boolean newData = false;        //New data valid flag

void setup() {
    pinMode(LED_BUILTIN, OUTPUT); // Onboard LED instellen
    digitalWrite(LED_BUILTIN, LOW); // Onboard LED uitzetten
    pinMode(CH1, OUTPUT);         // Set Channelpins as output 
    pinMode(CH2, OUTPUT);
    pinMode(CH3, OUTPUT);
    pinMode(CH4, OUTPUT);
    pinMode(CH5, OUTPUT);
    pinMode(CH_RESET, OUTPUT);
    
    digitalWrite(CH1, LOW);    // Set all used pins to low (0V)
    digitalWrite(CH2, LOW);
    digitalWrite(CH3, LOW);
    digitalWrite(CH4, LOW);
    digitalWrite(CH5, LOW);
    digitalWrite(CH_RESET, LOW);
    
    Serial.begin(9600); // Set USB serialport to 9600bd
    Serial.println("<Ready to receive commands @ 9600bd>"); // Report status thru serialport
    lastcommand[0] = '\0'; // Initialize last received command
}

void loop() {
    recvWithEndMarker(); // Wait for input on serialport
    showNewData(); // Show received data
    if (newData == true)
    {
      Docommand(); // Execute received command id newdata (command) is received
    }  
}

void recvWithEndMarker() {
    static byte ndx = 0;    // char 'pointer'
    char endMarker = '\n';  // newline charactr denotes the end of and input 
    char rc;                // Received character
   
    while (Serial.available() > 0 && newData == false) {    //Store data while serialport is available and newdata flag is false
        rc = Serial.read();  

        if (rc != endMarker) {                              // if  no endmarker (newline) is reveived store character
            receivedChars[ndx] = toupper(rc);               // Set all reveice characters to uppercase to make input not casesensitive
            ndx++;
            if (ndx >= numChars) {                          // Make sure not more then numChars (32) are put into the buffer
                ndx = numChars - 1;
            }
        }
        else {                                              // end marker received
            receivedChars[ndx] = '\0';                      // terminate the received string
            ndx = 0;                                        // reset charakter counter
            newData = true;                                 // 'raise' newdataflag to indicate there is newdata in the buffer
        }
    }
}


// Send the received data to the serialport for monitoring
void showNewData() {
    char status[80];
    
    if (newData == true) {
        memcpy(lastcommand,command,sizeof(command));          // Save the previous command
        memcpy(command,receivedChars,sizeof(receivedChars)); // Save curruntly received command in 'command'
        sprintf(status,"Previous command: %s, Last command: %s",lastcommand,command);
        Serial.println(status);
    }
}

// Execute the received command
// A channel will pulse high for 15ms which should be sufficient to set the latch relay in the RF switch, only 10ms should be required
// Although the hardware will prefend the output fron being high for to long it's best practice NOT to keep the output high
// A constant voltage on the relay will probably damage the coil in the RFswitch
void Docommand(){
 // The received command will only be executed when the previous command was "AT" (attention) to prefend inadvertend commands from being executed
  if (strcmp(lastcommand, "AT") == 0){
     if (strcmp(command, "CH1") == 0){
        digitalWrite(CH1, HIGH);
        delay(PULSE);
        digitalWrite(CH1, LOW);
     }
     else if (strcmp(command, "CH2") == 0){
        digitalWrite(CH2, HIGH);
        delay(PULSE);
        digitalWrite(CH2, LOW);  
     }
     else if (strcmp(command, "CH3") == 0){
        digitalWrite(CH3, HIGH);
        delay(PULSE);
        digitalWrite(CH3, LOW);
     }  
     else if (strcmp(command, "CH4") == 0){
        digitalWrite(CH4, HIGH);
        delay(PULSE);
        digitalWrite(CH4, LOW);
     }  
     else if (strcmp(command, "CH5") == 0){
        digitalWrite(CH5, HIGH);
        delay(PULSE);
        digitalWrite(CH5, LOW);
     }
     else if (strcmp(command, "CH6") == 0){
        digitalWrite(CH6, HIGH);
        delay(PULSE);
        digitalWrite(CH6, LOW);
     }  
     else if (strcmp(command, "CH_RESET") == 0){
        digitalWrite(CH_RESET, HIGH);
        delay(PULSE);
        digitalWrite(CH_RESET, LOW);
     }
     else if (strcmp(command, "VERSION") == 0){
      Serial.println(VERSION);
     }
          
  }
  newData = false; // reset newdata flag to indicate receive buffer is 'dirty' and can be overwriten
}
